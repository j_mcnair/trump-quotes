# Ask Chairman Trump
This is a kind of magic eight-ball for Trump, built as a web app.
 
The basic concept: Users can submit any question to “consult the wisdom of Trump” and as a response to that question the site will spit out a random Trump quote we’ve selected. Users can share their question and Trump’s “answer” via Twitter and Facebook, as a dynamic image; hopefully that user-specific sharing helps it spread, along the lines of PowerpuffYourself, Buzzfeed, etc. The site links at the bottom to the book.

It's built using Ruby on Rails, though that might not be the best architecture, but it was easy to set up.

The main customization right now is just static HTML, CSS, and some client-side Javascript.

The static HTML is in app/views/static_pages/index.html.erb
The HTML/CSS use Bootstrap, with some custom CSS in app/assets/stylesheets/static_pages.scss
The Javascript is inline in the HTML right now, which is probably wrong, but it was easier for me to set up.
There's also some scripts loaded in the <head/> of app/views/layouts/application.html.erb
The images are in app/assets/images. They're PNGs with transparency, hopefully small enough to keep performance OK without sacrificing desktop quality?

ACTION NEEDED:
1) Need to set up dynamic image sharing for social, to share the user's actual question and Trump's random response, with star illustration--just Facebook and Twitter. It's key to make each response a custom sharable image
2) Need to generate Facebook button and populate it with dynamic image sharing.
3) Need to edit current Twitter button to populate it with dynamic image sharing--and don't add multiple buttons when same user plays multiple times.
4) Need to add basic Facebook/Twitter share buttons on homepage only, under form input bar.
5) In footer, background needs stars--they work when tested locally, but not when I deploy using Heroku

If any of the above should change, go for it! I don't always know what I'm doing :).
